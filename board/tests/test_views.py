from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, APITestCase

from ..factories import SprintFactory
from ..models import Sprint
from ..serializers import SprintSerializer


class SprintViewTests(APITestCase):
    def test_get_sprint_list(self):
        sprint1 = SprintFactory(end='2016-05-02')
        sprint2 = SprintFactory(end='2016-05-03')
        url = reverse('sprint-list')

        expected = [
            {
                'id': sprint1.pk,
                'name': sprint1.name,
                'description': sprint1.description,
                'end': sprint1.end,
                'links': {
                    'self': 'http://testserver/api/sprints/{}/'.format(sprint1.pk),
                },
            },
            {
                'id': sprint2.pk,
                'name': sprint2.name,
                'description': sprint2.description,
                'end': sprint2.end,
                'links': {
                    'self': 'http://testserver/api/sprints/{}/'.format(sprint2.pk),
                },
            },
        ]
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, expected)

    def test_post_sprint_list(self):
        # given
        import datetime

        two_days_from_now = datetime.date.today() + datetime.timedelta(days=2)
        post_data = {'name': 'Some Sprint', 'end': two_days_from_now}
        url = reverse('sprint-list')

        # when
        response = self.client.post(url, data=post_data, format='json')

        # then
        sprint = Sprint.objects.get(pk=1)
        factory = APIRequestFactory()
        request = factory.post(url, data=post_data, format='json')
        # view = UserDetail.as_view()
        # response = view(request, pk=4)
        # response.render -> para response content ser criado.
        # assertequal response.content, {blablabla}
        context = {'request': request}
        serializer = SprintSerializer(sprint, context=context)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(response.data, serializer.data)

    def test_post_invalid_sprint(self):
        # TODO: try to post a sprint without 'end'
        pass

    def test_get_sprint_detail(self):
        sprint1 = SprintFactory()
        url = reverse('sprint-detail', kwargs={'pk': sprint1.pk})
        expected = {
            'id': sprint1.pk,
            'name': sprint1.name,
            'description': sprint1.description,
            'end': sprint1.end,
            'links': {
                'self': 'http://testserver/api/sprints/{}/'.format(sprint1.pk),
            },
        }

        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, expected)

    def test_get_sprint_detail_fail_for_wrong_id(self):
        # TODO: try pk=2 and get HTTP404.
        pass

    def test_put_sprint_detail(self):
        # TODO: write a test to change a sprint already in the db

        # TODO: write a test to change a sprint not in the db. Should fail.
        pass

    def test_delete_sprint_detail(self):
        # TODO: write a test to delete a sprint already in the db
        # TODO: write a test to delete a sprint not in the db. Should fail.
        pass

