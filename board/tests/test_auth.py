from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from ..factories import UserFactory
from django.contrib.auth.models import User


class AuthTests(APITestCase):
    def test_authenticate_user(self):
        self.given_i_am_an_user()
        self.when_i_post_to_api_token()
        self.then_i_should_get_my_token()

    def given_i_am_an_user(self):
        # given
        built_user = UserFactory.build()
        self.user = User.objects.create_user(
                username=built_user.username,
                email=built_user.email,
                password=built_user.password
                )

        self.data = {'username': self.user.username, 'password': built_user.password}

    def when_i_post_to_api_token(self):
        # when
        url = reverse('api-token')
        self.response = self.client.post(url, self.data, format='json')

    def then_i_should_get_my_token(self):
        # then
        token, _ = Token.objects.get_or_create(user=self.user)
        expected_response = {"token": token.key}
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.response.data, expected_response)


