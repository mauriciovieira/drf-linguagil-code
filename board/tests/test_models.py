from rest_framework.test import APITestCase

from ..factories import SprintFactory


class SprintModelTests(APITestCase):
    def test_sprint_string_representation_has_its_name(self):
        sprint = SprintFactory.build()
        sprint.end = None
        expected = "{}".format(sprint.name)
        self.assertEqual(str(sprint), expected)

    def test_sprint_string_representation_has_end_date(self):
        sprint = SprintFactory.build()
        sprint.name = None
        expected = "Sprint ending {}".format(sprint.end)
        self.assertEqual(str(sprint), expected)
