# from django.contrib.auth import get_user_model
from django.conf import settings
from django.db import models

User = settings.AUTH_USER_MODEL


class Sprint(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')
    description = models.TextField(blank=True, default='')
    end = models.DateField(unique=True)

    def __str__(self):
        return self.name or "Sprint ending %s" % (self.end)
